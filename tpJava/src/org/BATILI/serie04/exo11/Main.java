package org.BATILI.serie04.exo11;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class Main {
	public static void main(String[] args) {
		
		Function <Person ,String> getLastName = Person -> (Person.getLastName()).toUpperCase();
		Function <Person ,String> getfirstName = Person -> (Person.getFirstName()).toUpperCase();

		Function<String,Integer> chaineToLength = s -> s==null ? 0 : s.length();

		Comparator<String> comparer_deux_chaine= (s,v) -> s.length()-v.length();
		Comparator<String> comparerDeuxChaine = Comparator.comparing(chaineToLength);
		Comparator<String> comparerDeuxChaine2 = Comparator.comparing((String s) -> s);

		System.out.println(comparer_deux_chaine.compare("cc","bonjour")>0?true:false); 
		System.out.println(comparerDeuxChaine.compare("cc","bonjour")>0?true:false); 
		System.out.println(comparerDeuxChaine2.compare("cc","bonjour")<0?true:false); 
		System.out.println(comparerDeuxChaine2.compare("bonjour","cc")<0?true:false); 
		
		
       Comparator<Person> compare_LastName = Comparator.comparing(getLastName );
       Comparator<Person> compare_firstName = Comparator.comparing(getfirstName );
		
		Comparator<Person> compare_LastName_Then_firstName = compare_LastName.thenComparing(compare_firstName);
       
		Person hamza= new Person("Hamza","MOUHSINI",23);
		Person me= new Person("me","MOUHSINI",23);
		Person ayoub= new Person("Ayoub","TAOUSSI",23);	
		Person ismail= new Person("Ismail","BATILI",23);

	System.out.println("");
		
    System.out.println("hamza < ayoub :"+" "+(compare_LastName.compare(hamza,ayoub)<0));	
	System.out.println("hamza < ismail :"+" "+(compare_LastName.compare(hamza,ismail)<0));	
	System.out.println("ismail < ayoub :"+" "+(compare_LastName.compare(ismail,ayoub)<0));	
	System.out.println("hamza < me :"+" "+(compare_LastName.compare(hamza,me)<0)+"\n");
	

	
	System.out.println("hamza < ayoub :"+" "+(compare_LastName_Then_firstName.compare(hamza,ayoub)<0));	
	System.out.println("hamza < ismail :"+" "+(compare_LastName_Then_firstName.compare(hamza,ismail)<0));	
	System.out.println("ismail < ayoub :"+" "+(compare_LastName_Then_firstName.compare(ismail,ayoub)<0));	
	System.out.println("hamza < me :"+" "+(compare_LastName_Then_firstName.compare(hamza,me)<0)+"\n");
		
	Comparator<Person> compare_renverse =(compare_LastName_Then_firstName.reversed()) ;
	
	System.out.println("Comparaison inverse :");
	System.out.println("hamza < ayoub :"+" "+(compare_renverse.compare(hamza,ayoub)<0));	
	System.out.println("hamza < ismail :"+" "+(compare_renverse.compare(hamza,ismail)<0));	
	System.out.println("ismail < ayoub :"+" "+(compare_renverse.compare(ismail,ayoub)<0));	
	System.out.println("hamza < me :"+" "+(compare_renverse.compare(hamza,me)<0)+"\n");
		
	 List<Person> creerlist= new ArrayList<>();
		Person p1= new Person("hamza","mouhsini",23);
		Person p2= new Person("ayoub","Taoussi",23);
		Person p3= new Person("ismail","Batili",22);
		Person p4= new Person("me","mouhsini",23);	

	creerlist.add(p1);	
	creerlist.add(p2);	
	creerlist.add(p3);	
	creerlist.add(p4);	
	
	
	creerlist.sort(Comparator.nullsLast(compare_LastName_Then_firstName));
	
	creerlist.forEach(p -> System.out.println(p));
	
	
	}
	
}
