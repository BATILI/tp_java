package org.BATILI.serie04.exo11;
import java.util.Comparator;
import java.util.Objects;
import java.util.function.Function;

public interface comparator<T> 
{
static <T> Comparator<T> comparing(Function<T,String> keyExtractor){
	Objects.requireNonNull(keyExtractor);
	return (u1,u2)  -> keyExtractor.apply(u1).compareTo(keyExtractor.apply(u2));
	
}
	
	
}
