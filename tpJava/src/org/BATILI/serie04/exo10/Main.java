package org.BATILI.serie04.exo10;
import java.util.function.Function;
import java.util.function.BiFunction;

public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Function<String,String> chaine_maj= s-> s.toUpperCase();
		System.out.println(chaine_maj.apply("meme"));

		Function<String,String> chaine_return_meme_chose=s-> {
			if(s==null) return "";
			else return s;
		};
		System.out.println(chaine_return_meme_chose.apply(""));

		Function<String,Integer> longueur_chaine= s -> chaine_return_meme_chose.apply(s).length();
		System.out.println(longueur_chaine.apply("meme"));

		Function<String,String> chaine_parenthese= s-> "(" + chaine_return_meme_chose.apply(s) + ")";
		System.out.println(chaine_parenthese.apply(""));

		BiFunction<String,String,Integer> position_deuxieme_chaine= (x,y) -> x.indexOf(y);
	
		System.out.println(position_deuxieme_chaine.apply("ihkfdme","me"));

		Function<String,Integer> fonction_pos= s-> position_deuxieme_chaine.apply("abcdefghi",s);
		System.out.println(fonction_pos.apply("ab")); 


	}
}
