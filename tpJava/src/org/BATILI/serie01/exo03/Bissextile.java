package org.BATILI.serie01.exo03;

public class Bissextile {
	 public static boolean bissextile(int a) {
	        return (((a & 3) == 0) && ((a % 100 != 0) || (a % 400 == 0)));
	        
	    	} 
	 
	    public static void main(String[] args) 
	    {  
	        for(int n=1900;n<=2000;n++) {
	            if(bissextile(n)) System.out.print(n+" est une annee bissextile\n");
	        } 
	    }  
}
