package org.BATILI.serie01.exo02;
import java.math.BigInteger;
	
	public class Factorielle {
		
		BigInteger fact;

		    public static int intFactorielle(int n) {
		        int f = 1;
		        for (int i=1; i<=n; i++)
		        f=f*i;
		        return(f);
		    }
		    public static double doubleFactorielle(int n) {
		        double f = 1;
		        for (int i=1; i<=n; i++)
		        f=f*i;
		        return(f);
		    }

		    public BigInteger bigIntFactorielle(int n) {
		        fact=new BigInteger("1");
		        for (int i=1; i<=n; i++)
		        fact=fact.multiply(BigInteger.valueOf(i));
		        return(fact);
		    }
}
