package org.BATILI.serie01.exo04;

public class Palindrome {
	 public static boolean palindrome(String s) {
		 if(s.length() == 0 || s.length() == 1)
	         return true;

	     if(s.charAt(0) == s.charAt(s.length()-1))
	    	 return palindrome(s.substring(1, s.length()-1));
	     else return false;

	 }
	 public static void main(String[] args) {
	      String strings = "noan";

	         if(palindrome(strings))
	            System.out.println(" est un palindrome");
         else
             System.out.println(" n'est pas un palindrome");

	   }

}
