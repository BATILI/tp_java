package org.BATILI.serie08.exo16;
import org.BATILI.serie04.exo11.Person;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		Person p1 = new Person("ana", "batili", 23);
		Person p2 = new Person("papa", "ndiyae", 25);
		Person p3 = new Person("abdo", "balde", 24);
		List<Person> lstPerson = new ArrayList<>();
		lstPerson.add(p1);
		lstPerson.add(p2);
		lstPerson.add(p3);
		
		PersonBinWriter pWriter = new PersonBinWriter();
		pWriter.writeBinaryFields(lstPerson, "filePerson");
		
		PersonBinReader pReader = new PersonBinReader();
		pReader.readBinaryFields("filePerson");

	}

}
