package org.BATILI.serie06.exo14;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static List<String> readLinesFrom(String filename) {
		Path path = Paths.get(filename);
		try (Stream<String> lines = Files.lines(path)) {
		return lines.collect(Collectors.toList());
		} catch (IOException e) {
		System.out.println(e.getMessage());
		}
		return null;
		}
	
	public static void main(String[] args) {
				
		Function<String,Stream<Character>> string1 = s -> s.chars().mapToObj(c -> (char)c);
		
	 List <String> Germinal = readLinesFrom("C:\\Users\\ismai\\OneDrive\\Bureau\\7germ10.txt");
		List <String> listeline = Germinal.stream().limit(Germinal.size()-322).skip(70).collect(Collectors.toList());
     
		listeline.forEach(System.out::println);
		System.out.println("\n");
		System.out.println( " le nombre de lignes est : " + listeline.size() +" \n");
		
		long liste_nv = listeline.stream().filter(s -> !s.isEmpty()).count();
		
		System.out.println(" le nombre de lignes non vide "+liste_nv+" \n");
		
		BiFunction <String,String,Integer> bonjour = (s1,s2)->s1.split(s2).length-1;
		
		int nb_bonjour = listeline.stream().mapToInt(s -> bonjour.apply(s.toLowerCase(), "bonjour")).sum();
		
		System.out.println(" Le nombre de mot Bonjour "+nb_bonjour+"\n");
		
		Stream<Character> StrChar=string1.apply(Germinal.toString());
		
		System.out.println("les caracteres de Germinal sont : \n");
		System.out.println(StrChar.distinct().sorted().collect(Collectors.toList()));
    
		BiFunction<String,String,Stream<String>> splitWordWithPattern = (line,pattern)-> Pattern.compile("[" + pattern + "]").splitAsStream(line);
     String words= listeline.stream().filter(s->!s.isEmpty()).collect(Collectors.toList()).toString();
		System.out.println("Le nombre de mots est de : ");
		String ponctuation = " !#$%(\\)*+,-.\\/0123456789:;<>?\\@\\[\\]_~";
		System.out.println(+splitWordWithPattern.apply(words, ponctuation).count());
		
     String uniqueWords = listeline.stream().filter(s->!s.isEmpty()).distinct()	.collect(Collectors.toList()).toString();
     
     System.out.println("Le nombre de mots differents est de  : ");
		System.out.println(splitWordWithPattern.apply(uniqueWords, ponctuation).count());
    
		String mot_long = splitWordWithPattern.apply(uniqueWords, ponctuation).sorted(Comparator.comparing(String::length)).reduce((first, second) -> second).get().toString();
		System.out.println("le mot le plus long: "+mot_long);
		System.out.println("la longueur du mot le plus long: "+mot_long.length());
		
		int nb_mot_long = (int) splitWordWithPattern.apply(uniqueWords, ponctuation).filter(s->s.length()==mot_long.length()).distinct().count();
		System.out.println("nombre de mots les plus longs: "+nb_mot_long);
		splitWordWithPattern.apply(uniqueWords, ponctuation).filter(s->s.length()==mot_long.length()).distinct().forEach(s->System.out.println(s));

	
		
	}

}
