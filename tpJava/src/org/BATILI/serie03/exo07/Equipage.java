package org.BATILI.serie03.exo07;
import java.util.ArrayList;

import org.BATILI.serie02.exo05.Marin;

public class Equipage {
private ArrayList<Marin> equipage= new ArrayList<Marin>();
	

	public boolean addMarin(Marin marin)
	{
		
			return equipage.add(marin);

	}
	
	public boolean removeMarin(Marin marin)
	{
		return equipage.remove(marin);
	}
	
	
	public boolean isMarinPresent(Marin marin)
	{
		return equipage.contains(marin);
		
	}

	public void addAllEquipage(Equipage equipe)
	{
		Marin marin = new Marin ("","",0);
		for(int i=0;i<equipe.equipage.size();i++)
		{
			marin=equipe.equipage.get(i);
			if(!this.equipage.contains(marin))
				
				this.equipage.add(marin);
			
		}
		
	}
	
	public void clear(Equipage equipe)
	{
		
		for(int i=0;i<equipe.equipage.size();i++)
		{
			removeMarin(equipe.equipage.get(i));
		}
	}
	
	public int getNombreMarins(Equipage equipe)
	{
		
		return equipe.equipage.size();
	}
	
	
	public double getMoyenneSalaire(Equipage equipe)
	{
		int mean=0;
		for(int i=0;i<equipe.equipage.size();i++)
		{
			mean= mean+equipe.equipage.get(i).getSalaire();
		}
		return mean/getNombreMarins(equipe);
	}
	

	public Equipage() {
	}

	public String toString() {
		return "Equipage [equipage=" + equipage + "]";
	}


}
