package org.BATILI.serie03.exo07;
import java.util.ArrayList;

import org.BATILI.serie02.exo05.Marin;


	public class EquipageLimite {

		 private int nombre_max=4;
			private ArrayList<Marin> equipagelimite= new ArrayList<Marin>();
			

			public boolean addMarin(Marin marin)
			{
				if(equipagelimite.size()<this.nombre_max)
					return equipagelimite.add(marin);
	 		   else
				return false;
				
			}
			
			public boolean removeMarin(Marin marin)
			{
				return equipagelimite.remove(marin);
			}
			
			
			public boolean isMarinPresent(Marin marin)
			{
				return equipagelimite.contains(marin);
				
			}

			public void addAllEquipage(EquipageLimite equipe)
			{
				Marin marin = new Marin ("","",0);
				for(int i=0;i<equipe.equipagelimite.size();i++)
				{
					marin=equipe.equipagelimite.get(i);
				
					if (equipe.equipagelimite.size()<this.nombre_max)
						if (!this.equipagelimite.contains(marin))
					 
						this.equipagelimite.add(marin);
					}
					
				}
				
			

			public int getNombre_max() {
				return nombre_max;
			}

			public void setNombre_max(int nombre_max) {
				this.nombre_max = nombre_max;
			}
			public EquipageLimite (int nombre_max)
			{
				
			}

			public EquipageLimite() {
				// TODO Auto-generated constructor stub
			}

			
			@Override
			public String toString() {
				return "EquipageLimite [nombre_max=" + nombre_max + ", equipagelimite=" + equipagelimite + "]";
			}
			
}
