package org.BATILI.serie07.exo15;
import org.BATILI.serie04.exo11.Person;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PersonReader {

	Function<String, Person> lineToPerson = string -> new Person(string.split(",")[0],
																string.split(",")[1],
																Integer.parseInt(string.split(",")[2]));

	List<Person> read (String fileName) throws IOException{

		List<Person> persons = new ArrayList<>();
		File file = new File(fileName);
		try (FileReader fileReader = new FileReader(file);
				BufferedReader reader = new BufferedReader(fileReader);){

			persons = reader.lines()
					.filter(s->!s.startsWith("#") && !s.isEmpty())
					.map(lineToPerson)
					.collect(Collectors.toList());


		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}


		return persons;
	}
}
