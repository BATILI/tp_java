package org.BATILI.serie07.exo15;
import org.BATILI.serie04.exo11.Person;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;




public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PersonReader reader = new PersonReader();
		try {
			System.out.println("-------Reader--------");
			System.out.println(reader.read("fichier"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		Person p1 = new Person("ana", "batili", 23);
		Person p2 = new Person("papa", "ndiyae", 25);
		Person p3 = new Person("abdo", "balde", 24);

		List<Person> lstPerson = new ArrayList<>();
		lstPerson.add(p1);
		lstPerson.add(p2);
		lstPerson.add(p3);

		PersonWriter writer = new PersonWriter();
		if(writer.write(lstPerson, "fichier")){
			System.out.println("add success ");
		}
		
	}

}
