package org.BATILI.serie07.exo15;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;

import org.BATILI.serie04.exo11.Person;

public class PersonWriter {

		Function<Person, String> personToLine = p -> p.getLastName()+","+p.getFirstName()+","+p.getAge()+"\n";

		boolean write(List<Person> people, String fileName){

			File file = new File(fileName);

			try (FileWriter fileWriter = new FileWriter(file,true);
					BufferedWriter writer = new BufferedWriter(fileWriter);){

				for (Person person : people) {
					writer.write(personToLine.apply(person));
				}
				return true;
			}catch (IOException e1) {
				e1.printStackTrace();
				return false;
			}
			

		}
}
