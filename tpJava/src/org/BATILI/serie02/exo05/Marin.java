package org.BATILI.serie02.exo05;

public class Marin {

	  private String nom; 
	  private String prenom; 
	  private int salaire; 
	  
public Marin (String nom,String prenom,int salaire)	  {
	this.nom=nom;
	this.prenom=prenom;
	this.salaire=salaire;}

public Marin (String nom,int salaire)	  {
	this.nom=nom;
	this.salaire=salaire;}

public String getNom() {
	return nom;}

public void setNom(String nom) {
	this.nom = nom; }
public String getPrenom() {
	return prenom;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}
public int getSalaire() {
	return salaire;
}
public void setSalaire(int salaire) {
	this.salaire = salaire;
}
public  int augmentation( int augmentation) {
	return this.salaire + augmentation;
}
public String toString() {
	return "Marin [nom=" + nom + ", prenom=" + prenom + ", salaire=" + salaire + "]";
}


public  boolean equals(Object o) {  
  if (!(o  instanceof Marin))  
      return false ;  
   
 Marin marin = (Marin)o ;  
   
  return nom.equals(marin.nom) &&
        prenom.equals(marin.prenom);
       
}
public  int hashCode() {  
  int hashCode =  17 ;  
 hashCode =  31 * hashCode + ((nom == null) ?  0 : nom.hashCode()) ;  
 hashCode =  31 * hashCode + ((prenom == null) ?  0 : prenom.hashCode()) ;  
 hashCode =  31 * hashCode + salaire ;
  return hashCode ;
}

}

