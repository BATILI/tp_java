package org.BATILI.serie02.exo05;

public class Main {
	public static void main(String args[]) {
		Marin m1=new Marin("ismail","batili",9999);
		Marin m2= new Marin("ismail","batili",9999);
		Marin m3= new Marin("ayoub","taoussi",5555);
		System.out.println(m1.toString());
		System.out.println(m2.toString());
		System.out.println(m3.toString());
		if (m1.equals(m2))
			System.out.println("m1 & m2 sont egaux ");
		else System.out.println("m1 & m2 sont sont differents");
		if (m1.equals(m3))
			System.out.println("m1 & m3 sont egaux ");
		else System.out.println("m1 & m3 sont sont differents");
		if (m3.equals(m2))
			System.out.println("m3 & m2 sont egaux ");
		else System.out.println("m3 & m2 sont sont differents");
		System.out.println(m1.hashCode());
		System.out.println(m2.hashCode());
		System.out.println(m3.hashCode());
		System.out.println("m1 & m2 sont egaux au sens de equals() et retournent le meme hashCode()");
	}

}
