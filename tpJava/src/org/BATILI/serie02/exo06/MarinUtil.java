package org.BATILI.serie02.exo06;
import org.BATILI.serie02.exo05.Marin;
import java.util.Arrays;


public class MarinUtil {

    void augmenteSalaire(Marin[] marins,int pourcentage){

        for (Marin marin : marins) {

            marin.setSalaire(((marin.getSalaire()*pourcentage)/100)+marin.getSalaire());

            System.out.println(marin);

        }

    }

  
    int getMaxSalaire(Marin[] marins){

        int maxSalaire=0;

        for (Marin marin : marins) {

            if (marin.getSalaire()>maxSalaire)

                maxSalaire=marin.getSalaire();

        }

        return maxSalaire;

    }

    

    double getMoyenneSalire(Marin[] marins){

        double sommeSalaire = 0;

        for (Marin marin : marins) {

            sommeSalaire+=marin.getSalaire();

        }

        

        return (sommeSalaire/marins.length);

    }

    

    int getMedianeSalaire(Marin[] marins){

        int medSalaire=0;

        

        int[] tab= new int[marins.length];

        

        for (int i = 0;i <marins.length;i++) {

            tab[i]=marins[i].getSalaire();

        }

        

        Arrays.sort(tab);

        

        if((tab.length)%2==1){

            medSalaire=tab[(tab.length/2)];

        }

        else

            medSalaire=(tab[(tab.length/2)-1] +tab[(tab.length/2)+1])/2;

        

        return medSalaire;

    }

    

    

}